import re
import socket
import ssl
import time
from datetime import datetime
from functools import reduce
from queue import Queue
import simplefix as fix

from _thread import start_new_thread

ack = False
logoutSession = False
seq_num = 0
quoteCancel = False
fixData = Queue()
dataFormat = Queue()
tags_required = ["35", "55", "270"]

# host = '216.93.241.29'
# port = 33712
# SenderCompID = "PepperstoneInternal_Q"
# TargetCompID = "OZ_Q"
# password = '8t9WaZNt'

host = 'Pepperstone-live.onezero.com'
port = 32235
SenderCompID = 'QuantifAI_Q'
TargetCompID = 'Pepperstone_Q'
password = '96w3CRLp'

def ConstructHeader(msg_type):
    message = fix.FixMessage()
    global  seq_num
    seq_num += 1
    # Version of FIX
    message.append_pair(8, 'FIX.4.4', header=True)
    message.append_pair(35, msg_type)
    message.append_pair(49, SenderCompID)
    message.append_pair(56, TargetCompID)
    message.append_pair(34, seq_num)
    message.append_utc_timestamp(52, precision=3)
    return message


def LogonMessage(heartBeatSeconds, password, resetSeqNum):
    message = fix.FixMessage()
    message.append_pair(98, 0)
    message.append_pair(108, heartBeatSeconds)
    if resetSeqNum:
        message.append_pair(141, 'Y')
    message.append_pair(554, password)
    return message


def LogonPacket(resetSeqNum: bool=True) -> bytes:
    msg_type = 'A'
    message = ConstructHeader(msg_type)
    heartBeatSeconds = 30
    login_ = LogonMessage(heartBeatSeconds, password, resetSeqNum)
    message.append_strings(['='.join(map(lambda token: token.decode(), _)) for _ in login_.pairs])
    try:
        byte_buf = message.encode()
    except Exception as e:
        byte_buf = b''
        print(f"Ill-formed message {e}")
    return byte_buf


def HeartBeat(text):
    msg_type = '0'
    message = ConstructHeader(msg_type)
    if text:
        message.append_pair(112, text)
    try:
        byte_buf = message.encode()
    except Exception as e:
        byte_buf = b''
        print(f"Ill-formed message {e}")
    return byte_buf


def logout():
    msg_type = '5'
    message = ConstructHeader(msg_type)
    try:
        byte_buf = message.encode()
    except Exception as e:
        byte_buf = b''
        print(f"Ill-formed message {e}")
    return byte_buf


def MarketDataReq(subscription_id, instrument):
    msg_type = 'V'
    message = ConstructHeader(msg_type)
    message.append_pair(262, subscription_id)
    message.append_pair(263, 1)
    message.append_pair(264, 1)
    message.append_pair(265, 0)
    try:
        message.append_strings(['='.join(map(lambda token: token.decode(), _)) for _ in instrument.pairs])
    except Exception as e:
        print(f"{e} | {instrument}")
    message.append_pair(267, 2)
    message.append_pair(269, 0)
    message.append_pair(269, 1)
    try:
        byte_buf = message.encode()
    except Exception as e:
        byte_buf = b''
        print(f"Ill-formed message {e}")
    return byte_buf


def MarketDataCancel(ids, canType):
    msg_type = 'Z'
    message = ConstructHeader(msg_type)
    message.append_pair(298, canType)
    message.append_pair(117, ids)
    try:
        byte_buf = message.encode()
    except Exception as e:
        byte_buf = b''
        print(f"Ill-formed message {e}")
    return byte_buf


def _dataQueue(value_dict, security_bid_ask, trade_currencies):
    global ack, logoutSession, quoteCancel
    while True:
        data = dataFormat.get()
        index = 0
        length = len(data)
        quoteData = False
        value = 0
        bid = 0
        ask = 0
        bid_bool = True
        ask_bool = False
        while index < length:
            if data[index][0] == '35' and data[index][1] == 'W':
                quoteData = True
            elif data[index][0] == '35' and data[index][1] == '1':
                ack = True

            elif data[index][0] == '35' and data[index][1] == '5':
                print("LOGOUT from OneZero ...\n Trying to log back in")
                # TODO: add login session here
                login_packet = LogonPacket(False)
                try:
                    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    wrappedSocket = ssl.wrap_socket(s, ssl_version=ssl.PROTOCOL_TLSv1)
                    wrappedSocket.connect((host,port))
                    time.sleep(1)
                except (BrokenPipeError, OSError):
                    logoutSession = True
                    print("Recovery login unsucessful!")
                    return

            elif data[index][0] == '35' and data[index][1] == 'Z':
                quoteCancel = True
                print("quote cancelled")

            elif data[index][0] == '55':
                security = data[index][1]
                if quoteCancel is True:
                    print("quote cancelled ", security)
            elif data[index][0] == '270' and bid_bool is True:
                bid = float(data[index][1])
                ask_bool = True
                bid_bool = False
            elif data[index][0] == '270' and ask_bool is True:
                ask = float(data[index][1])
            index += 1
        if quoteData:
            try:
                security_data = dict(security_bid_ask[security])
                round_off = security_data["round"]
                value_dict[security_data["index"]]['bid'] = bid
                value_dict[security_data["index"]]['ask'] = ask
                value_dict[security_data["index"]]['ltp'].put(round(round_off*(bid + ask)/2)/round_off)
                value_dict[security_data["index"]]['minute_current'].put(datetime.utcnow().minute)
                security_bid_ask[security]["bid"] = bid
                security_bid_ask[security]["ask"] = ask
            except KeyError:
                pass
            if security in trade_currencies:
                trade_currencies[security] = bid

def tag_handler():
    tags = []
    parser = fix.FixParser()
    while True:
        parser.append_buffer(fixData.get())
        if parser.get_buffer() and b'\x0110=' in parser.get_buffer()[-8:]:
            try:
                cur_mesg = parser.get_message()
            except Exception as e:
                cur_mesg = None
                print(f"Current buffer: {parser.buf}\nError: {e}")
            if cur_mesg is not None:
                parser.reset()
                _tags = list(map(lambda x: (x[0].decode(), x[1].decode()), cur_mesg.pairs))
                for key, value in _tags:
                    if key in tags_required:
                        tags.append((key, value))
                    elif key == '10':
                        dataFormat.put(tags)
                        tags = []


def heartbeatRes(sock):
    global logoutSession
    while True:
        try:
            fixData.put(sock.recv())
        except (socket.timeout, TimeoutError, ConnectionResetError) as e:
            print(e, "Error")
            pass
        except OSError:
            break
        if logoutSession == True:
            break


def get_data(security_list):
    global  ack, logoutSession, quoteCancel

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    wrappedSocket = ssl.wrap_socket(s, ssl_version=ssl.PROTOCOL_TLSv1)
    wrappedSocket.connect((host,port))
    start_new_thread(heartbeatRes, (wrappedSocket,))
    start_new_thread(tag_handler, ())

    login_packet = LogonPacket()
    try:
        wrappedSocket.send(login_packet)
    except (BrokenPipeError, OSError):
        return

    time.sleep(1)
    start = time.time()
    instrument = fix.FixMessage()
    instrument.append_pair(146, len(security_list))
    [instrument.append_pair(55, sec) for sec in security_list]
    subscription_id = str(int(time.time()))
    login_packet = MarketDataReq(str(subscription_id), instrument)
    try:
        wrappedSocket.send(login_packet)
    except (BrokenPipeError, OSError):
        return

    time.sleep(1)

    while True:
        if time.time() - start > 29:
            start = time.time()
            heartbeat_packet = HeartBeat("")
            try:
                wrappedSocket.send(heartbeat_packet)
            except (BrokenPipeError, OSError):
                break

        if ack == True:
            ack = False
            heartbeat_packet = HeartBeat("TEST|")
            try:
                wrappedSocket.send(heartbeat_packet)
            except (BrokenPipeError, OSError):
                break

        if logoutSession is True:
            logout_packet = logout()
            try:
                wrappedSocket.send(logout_packet)
            except (BrokenPipeError, OSError, ConnectionResetError):
                pass
            break

        if quoteCancel is True:
            quoteCancel = False
    wrappedSocket.close()
    return


def main():
    get_data()

if __name__ == '__main__':
    main()
