import re
import socket
import ssl
import time
from datetime import datetime
from functools import reduce
from queue import Queue
from threading import Lock

import simplefix as fix

from _thread import start_new_thread

seq_trade_num = 0
lock = Lock()

# Live
host = 'Pepperstone-live.onezero.com'
port = 32235
SenderTradeCompID = 'QuantifAI_T'
TargetTradeCompID = 'Pepperstone_T'
password = '96w3CRLp'

# host = '216.93.241.29'
# port = 33712
# SenderTradeCompID = 'PepperstoneInternal_T'
# TargetTradeCompID = 'OZ_T'
# password = '8t9WaZNt'

clorderID = 0
fix_msg = Queue()
trade_details = Queue()

ack = False
logoutSession = False
loginSuccessful = True
order_info = None
security_map = None
account_balance = None


def ConstructHeader(msg_type, accountID=''):
    global  seq_trade_num
    lock.acquire()
    seq_trade_num += 1
    lock.release()
    message = fix.FixMessage()
    message.append_pair(8, 'FIX.4.4', header=True)
    message.append_pair(35, msg_type)
    message.append_pair(49, SenderTradeCompID)
    message.append_pair(56, TargetTradeCompID)
    message.append_pair(34, str(seq_trade_num), header=True)

    if accountID:
        message.append_pair(115, accountID)
    message.append_utc_timestamp(52, precision=3, header=True)
    return message


def LogonMessage(heartBeatSeconds, password, resetSeqNum):
    body = fix.FixMessage()
    # Encryption
    body.append_pair(98, 0)
    body.append_pair(108, heartBeatSeconds)

    if resetSeqNum:
        body.append_pair(141, 'Y')
    body.append_pair(554, password)
    return body


def LogonPacket():
    msg_type = 'A'
    global  seq_trade_num
    seq_trade_num = 0
    message = ConstructHeader(msg_type)
    heartBeatSeconds = 30
    resetSeqNum = 1
    login_ = LogonMessage(heartBeatSeconds, password, resetSeqNum)
    message.append_strings(['='.join(map(lambda token: token.decode(), _)) for _ in login_.pairs])
    try:
        byte_buf = message.encode()
    except Exception as e:
        byte_buf = b''
        print(f"Ill-formed message {e}")
    return byte_buf


def HeartBeat(text):
    msg_type = '0'
    message = ConstructHeader(msg_type)
    if text:
        message.append_pair(112, text)
    try:
        byte_buf = message.encode()
    except Exception as e:
        byte_buf = b''
        print(f"Ill-formed message {e}")
    return byte_buf


def LimitOrderReq(subscription_id, currency_pair, side, qty, price, ordType, accountID=''):
    msg_type = 'D'
    message = ConstructHeader(msg_type, accountID)
    global clorderID
    clorderID += 1

    message.append_pair(11, f'{datetime.now().strftime("%Y_%m_%d_%H_%M_%S")}_{clorderID}')
    message.append_pair(21, 1)
    message.append_pair(55, currency_pair)
    message.append_pair(54, side)
    message.append_pair(38, qty)
    message.append_pair(40, ordType)
    message.append_pair(44, price)
    message.append_pair(59, 1)
    message.append_utc_timestamp(60, precision=3)

    try:
        byte_buf = message.encode()
    except Exception as e:
        byte_buf = b''
        print(f"Ill-formed message {e}")
    return byte_buf


def orderCancellation(origClorderID, side, transact_time, accountID=''):
    msg_type = 'F'
    message = ConstructHeader(msg_type, accountID)
    global clorderID
    clorderID += 1
    if transact_time:
        message.append_pair(41, origClorderID)
        message.append_pair(11, f'{datetime.now().strtime("%Y_%m_%d_%H_%M_%S")}_{clorderID}')
        message.append_pair(54, side)
        message.append_pair(60, transact_time)
        try:
            byte_buf = message.encode()
        except Exception as e:
            byte_buf = b''
            print(f"Ill-formed message {e}")
        return byte_buf


def account_info_tag():
    msg_type = 'BB'
    message = ConstructHeader(msg_type)
    message.append_pair(909, int(time.time()))
    message.append_pair(453, 1)
    message.append_pair(448, '*')
    try:
        byte_buf = message.encode()
    except Exception as e:
        byte_buf = b''
        print(f"Ill-formed message {e}")
    return byte_buf


def logout():
    msg_type = '5'
    message = ConstructHeader(msg_type)
    try:
        byte_buf = message.encode()
    except Exception as e:
        byte_buf = b''
        print(f"Ill-formed message {e}")
    return byte_buf


def trade_handler(order_dict, account_info):
    global loginSuccessful, ack, logoutSession, account_balance
    while True:
        data = trade_details.get()
        data_feed = dict(data)
        if data_feed['35'] == '8':
            cur_sym = data_feed['55']
            curr_account = account_info[int(cur_sym.split('_')[-1]) - 1]
            security_info = order_dict[data_feed['55']]
            security_info['updated'] = True
            security_info['clOrderID'] = data_feed['11']
            security_info['orderID'] = data_feed['37']
            security_info['orderStatus'] = data_feed['39']
            security_info['execType'] = data_feed['150']
            security_info['side'] = int(data_feed['54'])
            security_info['orderQty'] = int(data_feed['38'])
            try:
                security_info['requestedPrice'] = float(data_feed['44'])
            except:
                print("data for this trade", data_feed)
                security_info['requestedPrice'] = float(data_feed['6'])

            security_info['remainingQty'] = int(data_feed['151'])
            security_info["cummQty"] = int(data_feed['14'])
            security_info['avgPrice'] = float(data_feed['6'])
            try:
                security_info['transactTime'] = data_feed['60']
            except:
                security_info['transactTime'] = None

            if data_feed['39'] == 'A':
                print("order pending")
            elif data_feed['39'] == '2':
                print('order completed')
                # DIVIDE TOTAL RISK / 40
                # cost per frac * delta in entry and exit price

            elif data_feed['39'] == '1':
                # price per lot TOTAL RISK / LOTS
                # UPDATED_RISK = qty_satis * price per lot
                # DIVIDE UPDATED RISK / 40
                # cost per frac * delta in entry and exit price
                print("partially FIlled ")
                # n_lots = (security_info['remainingQty'] + security_info['cummQty']) / 100000
                # price_per_lot = balance / n_lots
                # new_risk = security_info['cummQty'] * price_per_lot
                # price_per_pip = new_risk / 40
                # curr_account['balance'] += price_per_pip * (security_info['requestedPrice'] - security_info['avgPrice'])
            elif data_feed['39'] == '4':
                print("canclled Ordered")
            elif data_feed['39'] == '8':
                print("rejected ")
        elif data_feed['35'] == 'BA':
            # equity_balance = balance +/- unrealized
            try:
                account_balance[data_feed["448"]]['balance'] = float(data_feed['8881'])
                account_balance[data_feed["448"]]['equity_balance'] = float(data_feed['8886'])
                account_balance[data_feed["448"]]['unrealized'] = float(data_feed['8885'])
            except KeyError:
                pass
        elif data_feed['35'] == '5':
            try:
                if data_feed['58'] == "Rejected Logon Attempt: Invalid Credentials (error 3)":
                    print("|58=Rejected Logon Attempt: Invalid Credentials (error 3)|")
                    loginSuccessful = False
            except Exception as e:
                print(e, "logout")
                logoutSession = True


def tag_handler():
    tags = []
    parser = fix.FixParser()
    while True:
        parser.append_buffer(fix_msg.get())
        if parser.get_buffer() and b'\x0110=' in parser.get_buffer()[-8:]:
            try:
                cur_mesg = parser.get_message()
            except Exception as e:
                cur_mesg = None
                print(f"Current buffer: {parser.buf}\nError: {e}")
            if cur_mesg is not None:
                parser.reset()
                _tags = list(map(lambda x: (x[0].decode(), x[1].decode()), cur_mesg.pairs))
                for key, value in _tags:
                    if key == '10':
                        trade_details.put(tags)
                        tags = []
                    else:
                        tags.append((key, value))

def socket_recv(sock):
    global logoutSession
    while True:
        try:
            fix_msg.put(sock.recv())
        except (socket.timeout, TimeoutError) as e:
            print(e, "Error")
            pass
        except OSError:
            break
        if logoutSession == True:
            break


def place_order(socketT, trade_queue, account_info):
    global order_info
    while True:
        trade = trade_queue.get()
        print("Trade ", trade)
        if trade['type'] == 0:
            message = LimitOrderReq(trade['subscription_id'], trade['currency_pair'], trade['side'], trade['qty'], trade['price'],
                          trade['ordType'], trade['accountID'])
            try:
                # socketT.send(message)  # FIXME: uncomment for live exec
                open('trade_log', 'a+').write(message.decode())
            except (BrokenPipeError, OSError):
                print("Failed to place the order")
                trade_queue.put(trade)
                break
        else:
            currency_pair = trade['currency_pair']
            message = orderCancellation(order_info[currency_pair]['clOrderID'], order_info[currency_pair]['side'],
                order_info[currency_pair]['transactTime'], order_info[currency_pair]['accountID'])
            try:
                # socketT.send(message)  # FIXME: uncomment for live exec
                open('trade_log', 'a+').write(message.decode())
            except (BrokenPipeError, TypeError, OSError):
                print("Failed to Cancel the order")
                trade_queue.put(trade)
                break

# FIXME: potentially remove
# def account_balance_handler(socketT):
#     start = time.time()
#     while True:
#         if time.time() -start > 50:
#             start = time.time()
#             try:
#                 socketT.send(account_info_tag())
#             except (BrokenPipeError, OSError):
#                 break



def trade_session(trade_queue, order_dict, account_info):
    global order_info, account_balance
    order_info = order_dict
    account_balance = account_info
    session_trade = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    socketT = ssl.wrap_socket(session_trade, ssl_version=ssl.PROTOCOL_TLSv1)
    socketT.connect((host, port))

    start_new_thread(socket_recv, (socketT,))
    start_new_thread(tag_handler, ())
    start_new_thread(place_order, (socketT, trade_queue, account_info))
    start_new_thread(trade_handler, (order_dict, account_info))
    # start_new_thread(account_balance_handler, (socketT,))  # FIXME: potentially remove


    global ack, logoutSession
    login_packet = LogonPacket()
    try:
        socketT.send(login_packet)
    except (BrokenPipeError, OSError):
        return
    time.sleep(1)
    if loginSuccessful is False:
        print("Unable to start trade session")
        exit()

    start = time.time()
    logoutTime = time.time()
    while True:
        if time.time() - start > 27:
            start = time.time()
            heart_packet = HeartBeat("")
            try:
                socketT.send(heart_packet)
            except (BrokenPipeError, OSError):
                break

        if ack == True:
            ack = False
            heart_packet = HeartBeat("TEST|")
            try:
                socketT.send(heart_packet)
            except (BrokenPipeError, OSError):
                break

        if logoutSession is True:
            logout_packet = logout()
            try:
                socketT.send(logout_packet)
            except (BrokenPipeError, OSError):
                pass
            break
    time.sleep(4)
    socketT.close()


def main():
    trade_session(Queue())


if __name__ == '__main__':
    main()
