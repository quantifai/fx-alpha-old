import os
import time
from datetime import datetime

import pandas as pd

from _thread import start_new_thread
from utils import position, riskLimit

LOG_FILE = 'trade_log'
ema = lambda period, current, prevema: current * (2.0 / (period + 1.0)) + prevema * (1.0 - (2.0 / (period + 1.0)))

def cal_bollinger(close_prices):
    middle_value = sum(close_prices) / len(close_prices)
    std = 0
    for price in close_prices:
        std += (middle_value - price)**2
    std = (std / len(close_prices))**0.5
    return round(middle_value - 2*std, 6), round(middle_value + 2*std, 6)


def cal_renko(sec_dict, security_bid_ask, order_queue, account_info, rolling_trades):
    trade_cols = ['timestamp', 'type', 'pair', 'position', 'ltp', 'entry', 'target', 'stoploss', 'bal_before', 'bal_after']
    ltp = sec_dict['ltp'].get()
    curr_time = sec_dict['minute_current'].get()
    curr_account = account_info[int(sec_dict['accountID'].split('_')[-1]) - 1]
    LOG_ENTRY = None
    # TODO: change all `accountID`s to single with internal representation
    # If a position has been occupied check whether an order can be placed
    if sec_dict['signal'] is not position.NOPOS and sec_dict['position_filled'] is not False:

        # | Type    | Condition         | Outcome   |  UPDATE     |
        # | LONG    | LTP >= TARGET     | PROFIT    | LTP - ENTRY |
        # | LONG    | LTP <= STOPLOSS   | LOSS      | LTP - ENTRY |
        # | SHORT   | LTP <= TARGET     | PROFIT    | ENTRY - LTP |
        # | SHORT   | LTP >= STOPLOSS   | LOSS      | ENTRY - LTP |

        bal_before_trade = curr_account['balance']
        price_per_frac = (0.01 * bal_before_trade) / 40

        # long position
        if sec_dict['signal'] == position.LONG:
            price_delta = (ltp - sec_dict['entry']) / sec_dict['pip']

            if ltp >= sec_dict['target']:  # profit
                order_queue.put({'type': 0, 'subscription_id': time.time(), 'currency_pair': sec_dict['epic'], 'side': 2, 'qty': sec_dict['ordered_lots'], 'price': sec_dict['bid'], 'ordType': 1, 'accountID': sec_dict['accountID']})

                curr_account['balance'] += price_per_frac * (price_delta * 10)
                LOG_ENTRY = f"[PROFIT] {sec_dict['epic']} LONG @ {ltp} | E{sec_dict['entry']} T{sec_dict['target']} S{sec_dict['stoploss']} || Balance: {bal_before_trade} => {curr_account['balance']}"
                sec_dict['signal'] = position.NOPOS

            elif ltp <= sec_dict['stoploss']:  # loss
                order_queue.put({'type': 0, 'subscription_id': time.time(), 'currency_pair': sec_dict['epic'], 'side': 2, 'qty': sec_dict['ordered_lots'], 'price': sec_dict['bid'], 'ordType': 1, 'accountID': sec_dict['accountID']})

                curr_account['balance'] += price_per_frac * (price_delta * 10)
                LOG_ENTRY = f"[LOSS] {sec_dict['epic']} LONG @ {ltp} | E{sec_dict['entry']} T{sec_dict['target']} S{sec_dict['stoploss']} || Balance: {bal_before_trade} => {curr_account['balance']}"
                sec_dict['signal'] = position.NOPOS

        # short position
        elif sec_dict['signal'] == position.SHORT:
            price_delta = (sec_dict['entry'] - ltp) / sec_dict['pip']

            if ltp <= sec_dict['target']:  # profit
                order_queue.put({'type': 0, 'subscription_id': time.time(), 'currency_pair': sec_dict['epic'], 'side': 1, 'qty': sec_dict['ordered_lots'], 'price': sec_dict['ask'], 'ordType': 1, 'accountID': sec_dict['accountID']})

                curr_account['balance'] += price_per_frac * (price_delta * 10)
                LOG_ENTRY = f"[PROFIT] {sec_dict['epic']} SHORT @ {ltp} | E{sec_dict['entry']} T{sec_dict['target']} S{sec_dict['stoploss']} || Balance: {bal_before_trade} => {curr_account['balance']}"
                sec_dict['signal'] = position.NOPOS

            elif ltp >= sec_dict['stoploss']:  # loss
                order_queue.put({'type': 0, 'subscription_id': time.time(), 'currency_pair': sec_dict['epic'], 'side': 1, 'qty': sec_dict['ordered_lots'], 'price': sec_dict['ask'], 'ordType': 1, 'accountID': sec_dict['accountID']})

                curr_account['balance'] += price_per_frac * (price_delta * 10)
                LOG_ENTRY = f"[LOSS] {sec_dict['epic']} SHORT @ {ltp} | E{sec_dict['entry']} T{sec_dict['target']} S{sec_dict['stoploss']} || Balance: {bal_before_trade} => {curr_account['balance']}"
                sec_dict['signal'] = position.NOPOS

        if LOG_ENTRY is not None:
            print(LOG_ENTRY)
            entry = f"{time.time()} :: {LOG_ENTRY}\n"
            open(LOG_FILE, 'a+').write(entry)  # log it to a file
            rolling_trades.append(entry)  # add new row (for DB storage)

    if sec_dict['started'] is False:
        if curr_time != sec_dict['minute_prev_current']:
            sec_dict['minute_prev_current'] = curr_time
            sec_dict['minute_open'] = ltp
            sec_dict['minute_low'] = ltp
            sec_dict['minute_high'] = ltp
            sec_dict['started'] = True

    elif curr_time != sec_dict['minute_prev_current']:
        if sec_dict['position_filled'] is False:
            sec_dict['position_filled'] = True
            pos_held = 'LONG' if sec_dict['signal'] == position.LONG else 'SHORT'
            sec_dict['signal'] = position.NOPOS
            # order_queue.put({'type': 1, 'currency_pair': sec_dict['epic']})
            CLOSE_MESSAGE = f"{time.time()} :: [CLOSED] {sec_dict['epic']} {pos_held} | NON-BREACH"
            print(CLOSE_MESSAGE)
            open(LOG_FILE, 'a+').write(f"{CLOSE_MESSAGE}\n")

        sec_dict['minute_prev_current'] = curr_time
        sec_dict['minute_frame'].append(
            {'open': sec_dict['minute_open'],
            'high': sec_dict['minute_high'],
            'low': sec_dict['minute_low'],
            'close': sec_dict['minute_close'],
            'time': curr_time - 1,
            "hour": datetime.now().hour,
            'epic': sec_dict['epic']})

        print(sec_dict['minute_frame'][-1])
        sec_dict['minute_open'] = ltp
        sec_dict['minute_low'] = ltp
        sec_dict['minute_high'] = ltp

        close_price = list(map(lambda x: x['close'], sec_dict['minute_frame']))

        if len(sec_dict['minute_frame']) > 16:
            sec_dict['ema_16'] = ema(16, close_price[-1], sec_dict['ema_16'])
            print(f"ema_16 \t {sec_dict['ema_16']}")

        elif len(sec_dict['minute_frame']) == 16:
            sec_dict['ema_16'] = sum(close_price)/ 16
            print(f"ema_16 \t {sec_dict['ema_16']}")

        if len(sec_dict['minute_frame']) > 28:
            sec_dict['ema_28'] = ema(28, close_price[-1], sec_dict['ema_28'])
            sec_dict['macd'].append(sec_dict['ema_16'] - sec_dict['ema_28'])
            print(f"ema_16 {sec_dict['ema_16']} \t ema_28 {sec_dict['ema_28']} \t macd {sec_dict['macd'][-1]}")

        elif len(sec_dict['minute_frame']) == 28:
            sec_dict['ema_28'] = sum(close_price)/ 28
            sec_dict['macd'].append(sec_dict['ema_16'] - sec_dict['ema_28'])
            print("ema_16 {} \t ema_28 {} \t macd {}".format(sec_dict['ema_16'], sec_dict['ema_28'], sec_dict['macd'][-1]))

        if len(sec_dict['macd']) > 6:
            sec_dict['macdSignal'] = ema(6, sec_dict['macd'][-1], sec_dict['macdSignal'])
            sec_dict['macdHisto'].append(round(sec_dict['macd'][-1] - sec_dict['macdSignal'], 7))
            print(f"signal {sec_dict['macdSignal']} \t MACDHISTO {sec_dict['macdHisto'][-1]}")

        elif len(sec_dict['macd']) == 6:
            sec_dict['macdSignal'] = sum(sec_dict['macd'])/ 6
            sec_dict['macdHisto'].append(round(sec_dict['macd'][-1] - sec_dict['macdSignal'], 7))
            print(f"signal {sec_dict['macdSignal']} \t MACDHISTO {sec_dict['macdHisto'][-1]}")

        if len(sec_dict['minute_frame']) >= 20:
            boll_down, boll_up = cal_bollinger(list(map(lambda x: x['close'], sec_dict['minute_frame']))[-20:])
            sec_dict['bollinger_low'].append(boll_down)
            sec_dict['bollinger_high'].append(boll_up)

            print(f"boll up {sec_dict['bollinger_high'][-1]} , boll down {sec_dict['bollinger_low'][-1]}")

        if len(sec_dict['minute_frame']) > 38:
            SIGNAL_MESSAGE = None
            longpierce = sec_dict['minute_frame'][-2]['low'] < sec_dict['bollinger_low'][-4] or \
                sec_dict['minute_frame'][-3]['low'] < sec_dict['bollinger_low'][-4] or \
                sec_dict['minute_frame'][-4]['low'] < sec_dict['bollinger_low'][-4]

            shortpierce = sec_dict['minute_frame'][-2]['high'] > sec_dict['bollinger_high'][-2] or \
                sec_dict['minute_frame'][-3]['high'] > sec_dict['bollinger_high'][-3] or \
                sec_dict['minute_frame'][-4]['high'] > sec_dict['bollinger_high'][-4]

            longleftPyramid = sec_dict['macdHisto'][-1] < 0 and sec_dict['macdHisto'][-2] <= 0 and \
                              sec_dict['macdHisto'][-3] <= 0 and sec_dict['macdHisto'][-4] <= 0 and \
                              sec_dict['macdHisto'][-1] > sec_dict['macdHisto'][-2] and \
                              sec_dict['macdHisto'][-2] <= sec_dict['macdHisto'][-3] <= sec_dict['macdHisto'][-4]

            shortleftPyramid = sec_dict['macdHisto'][-1] > 0 and sec_dict['macdHisto'][-2] >= 0 and \
                              sec_dict['macdHisto'][-3] >= 0 and sec_dict['macdHisto'][-4] >= 0 and \
                              sec_dict['macdHisto'][-1] < sec_dict['macdHisto'][-2] and \
                              sec_dict['macdHisto'][-2] >= sec_dict['macdHisto'][-3] >= sec_dict['macdHisto'][-4]

            minbolldistance = (sec_dict['bollinger_high'][-1] - sec_dict['bollinger_low'][-1]) >= 4*sec_dict['pip']

            if longleftPyramid and longpierce:
                longleftsignal = True
                longturnpoint = sec_dict['minute_frame'][-1]['open']
            else:
                longleftsignal = False

            if shortleftPyramid and shortpierce:
                shortleftsignal = True
                shortturnpoint = sec_dict['minute_frame'][-1]['open']
            else:
                shortleftsignal = False

            if longleftsignal is True and minbolldistance is True and sec_dict['signal'] == position.NOPOS and sec_dict['stop_trades'] is False:
                sec_dict['signal'] = position.LONG
                sec_dict['entry'] = longturnpoint
                sec_dict['stopfrac'] = sec_dict['SLmulitple'] * sec_dict['pip']
                SIGNAL_MESSAGE = f"{time.time()} :: [SIGNAL] {sec_dict['epic']} LONG @ {sec_dict['entry']}"
                sec_dict['position_filled'] = False
            if shortleftsignal is True and minbolldistance is True and sec_dict['signal'] == position.NOPOS and sec_dict['stop_trades'] is False:
                sec_dict['signal'] = position.SHORT
                sec_dict['entry'] = shortturnpoint
                sec_dict['stopfrac'] = sec_dict['SLmulitple'] * sec_dict['pip']
                SIGNAL_MESSAGE = f"{time.time()} :: [SIGNAL] {sec_dict['epic']} SHORT @ {sec_dict['entry']}"
                sec_dict['position_filled'] = False

            if SIGNAL_MESSAGE is not None:
                print(SIGNAL_MESSAGE)
                open(LOG_FILE, 'a+').write(f"{SIGNAL_MESSAGE}\n")

    else:
        sec_dict['minute_high'] = max(sec_dict['minute_high'], ltp)
        sec_dict['minute_low'] = min(sec_dict['minute_low'], ltp)
        sec_dict['minute_close'] = ltp

    if sec_dict['position_filled'] is False:
        POSITION_MESSAGE = None
        if sec_dict['signal'] == position.LONG and sec_dict['entry'] >= ltp:
            lots = sec_dict['lots']
            order_queue.put({'type': 0, 'subscription_id': time.time(), 'currency_pair': sec_dict['epic'], 'side': 1, 'qty': lots, 'price': sec_dict['bid'], 'ordType': 2, 'accountID': sec_dict['accountID']})
            account_info
            sec_dict['ordered_lots'] = lots
            sec_dict["stoploss"] = ltp - sec_dict["stopfrac"]
            sec_dict['target'] = ltp + (sec_dict['stopfrac'] * riskLimit.profitTarget.value)
            POSITION_MESSAGE = f"{time.time()} :: [POSITION] {sec_dict['epic']} LONG @ {ltp} || S:{sec_dict['stoploss']}\t<->\tT:{sec_dict['target']}"
            sec_dict['position_filled'] = True

        elif sec_dict['signal'] == position.SHORT and sec_dict['entry'] <= ltp:
            lots = sec_dict['lots']
            order_queue.put({'type': 0, 'subscription_id': time.time(), 'currency_pair': sec_dict['epic'], 'side': 2, 'qty': lots, 'price': sec_dict['bid'], 'ordType': 2, 'accountID': sec_dict['accountID']})
            sec_dict['ordered_lots'] = lots
            sec_dict["stoploss"] = ltp + sec_dict["stopfrac"]
            sec_dict['target'] = ltp - (sec_dict['stopfrac'] * riskLimit.profitTarget.value)
            POSITION_MESSAGE = f"SHORT POSITION {sec_dict['epic']} <-> {datetime.now()} @ {ltp} | S:{sec_dict['stoploss']} :: T:{sec_dict['target']}"
            sec_dict['position_filled'] = True

        if POSITION_MESSAGE is not None:
            print(POSITION_MESSAGE)
            open(LOG_FILE, 'a+').write(f"{POSITION_MESSAGE}\n")

    return account_info, rolling_trades
