#!/usr/bin/env python3
"""
    :brief: Interface function to call on the fx-alpha strategy
"""
import os
from collections import deque
from datetime import datetime
import json
from queue import Queue
from time import sleep, time

import pandas as pd

from _thread import start_new_thread
from getData import _dataQueue, get_data
from mail_trade import send_mail
from strategy import cal_renko
from trade_session import trade_session
from utils import position, Tupperware, format_parse
import sqlalchemy

RISKLEVEL = {
    "mayank": 0.1,
    "sampath": 0.05,
    "gerard": 0.01
}

epics = Tupperware({
    "EURUSD": {'currencyCode':"USDUSD", 'pip': 0.0001, 'perlot': 100000, "accountID": 'QuantifAI_1' , 'lotexponent': 1 },
    "USDCAD": {'currencyCode':"USDCAD", 'pip': 0.0001, 'perlot': 100000, "accountID": 'QuantifAI_2' , 'lotexponent': 1 },
    "NZDJPY": {'currencyCode':"USDJPY", 'pip': 0.0100, 'perlot': 100000, "accountID": 'QuantifAI_3' , 'lotexponent': 1 },
    "EURAUD": {'currencyCode':"AUDUSD", 'pip': 0.0001, 'perlot': 100000, "accountID": 'QuantifAI_4' , 'lotexponent': -1},
    "EURJPY": {'currencyCode':"USDJPY", 'pip': 0.0100, 'perlot': 100000, "accountID": 'QuantifAI_5' , 'lotexponent': 1 },
    "GBPUSD": {'currencyCode':"USDUSD", 'pip': 0.0001, 'perlot': 100000, "accountID": 'QuantifAI_6' , 'lotexponent': 1 },
    "EURGBP": {'currencyCode':"GBPUSD", 'pip': 0.0001, 'perlot': 100000, "accountID": 'QuantifAI_7' , 'lotexponent': -1},
    "USDCHF": {'currencyCode':"USDCHF", 'pip': 0.0001, 'perlot': 100000, "accountID": 'QuantifAI_8' , 'lotexponent': 1 },
    "EURCAD": {'currencyCode':"USDCAD", 'pip': 0.0001, 'perlot': 100000, "accountID": 'QuantifAI_9' , 'lotexponent': 1 },
    "AUDJPY": {'currencyCode':"USDJPY", 'pip': 0.0100, 'perlot': 100000, "accountID": 'QuantifAI_10', 'lotexponent': 1 },
})

trade_currencies= Tupperware({
    "USDUSD": 1,
    "USDCAD": 0,
    "USDJPY": 0,
    "AUDUSD": 0,
    "GBPUSD": 0,
    "USDCHF": 0
})

# creating the accounts
CAPTIAL = 3.4e5
account_info = Tupperware()
N_ACCOUNTS = 10
offset = 0
for _ in range(N_ACCOUNTS):
    current_account = Tupperware()
    current_account['name'] = f"QuantifAI_{_ + 1}"
    current_account['balance'] = CAPTIAL / N_ACCOUNTS
    current_account['equity_balance'] = 0.0
    current_account['unrealized'] = 0.0
    account_info[offset + _] = current_account  # add the new account

# atr_period = 14
# ema6 = 6
# ema18 = 18
# total_wait = 19
order_queue = Queue()
engine = sqlalchemy.create_engine(os.environ.get('DATABASE_URL', 'sqlite://'))


def forex(value_dict, epics, security_bid_ask, account_info):
    """Run cal_renko given current security_bid_ask

    :param value_dict: JSON -> value_dict
    :param epics: JSON -> ref epics
    :param security_bid_ask: JSON -> security bid values
    """
    trade_cols = ['timestamp', 'type', 'pair', 'position', 'ltp', 'entry', 'target', 'stoploss', 'bal_before', 'bal_after']
    format_str = '{timestamp} :: [{type}] {pair} {position} @ {ltp} | E{entry} T{target} S{stoploss} || Balance: {bal_before} => {bal_after}'
    trade_df = pd.DataFrame([], columns=trade_cols)
    rolling_trades = []  # fill with dicts
    while True:
        for index, item in enumerate(epics):
            if not value_dict[index]['ltp'].empty():
                account_info, rolling_trades = cal_renko(value_dict[index], security_bid_ask,
                    order_queue, account_info, rolling_trades)

        if(len(rolling_trades)) >= 1000:  # write to the database
                # TODO: add transaction update here
                trades = [format_parse(trade, format_str) for trade in rolling_trades]
                trade_df = trade_df.append(trades)
                trade_df.timestamp = trade_df.timestamp.map(lambda times_: datetime.utcfromtimestamp(float(times_)))
                numeric_fields = ['ltp', 'entry', 'target', 'stoploss', 'bal_before', 'bal_after']
                for field in numeric_fields:
                    trade_df[field] = pd.to_numeric(trade_df[field])
                trade_df.to_sql('trades', engine, if_exists='append', index=False)
                # reset mem stores
                rolling_trades = []
                trade_df = trade_df.iloc[0:0]  # empty the database


def flat_zone(value_dict, security_bid_ask):
    while True:
        df = pd.read_sql("red_zones", engine)
        for index, row in df.iterrows():
            try:
                date = f"{row['date'].date()} {row['start_time']}"
                start_timestamp = datetime.strptime(date, "%Y-%m-%d %H:%M")
                date = f"{row['date'].date()} {row['end_time']}"
                end_timestamp = datetime.strptime(date, "%Y-%m-%d %H:%M")
                value_dict[security_bid_ask[row['currency_pair']]['index']]['stop_trades'] = False
                if datetime.utcnow() > start_timestamp and datetime.utcnow() < end_timestamp:
                    value_dict[security_bid_ask[row['currency_pair']]['index']]['stop_trades'] = True
            except KeyError:
                pass
        sleep(300)


def quote_session_handler(value_dict, security_bid_ask, currency_pairs, trade_currencies):
    start_new_thread(_dataQueue, (value_dict, security_bid_ask, trade_currencies))
    while True:
        get_data(list(currency_pairs))
        sleep(3)


def trade_session_handler(order_dict: Tupperware):
    """Control the open positions depending on current state

    :param order_dict: JSON -> current set of orders
    """
    while True:
        trade_session(order_queue, order_dict, account_info)
        sleep(3)


def interface_handler(order_dict: Tupperware):
    """Update balance and trades into the database at intervals

    :param order_dict: JSON -> current set of orders
    """
    start = time()
    while True:
        if time() - start > 60:
            start = time()
            df_account = pd.DataFrame(account_info).T
            try:
                df_account.to_sql('v_balances', engine, if_exists='replace', index=False)
            except:
                print("DB write failed!")
                print(df_account.head())

        for security in order_dict:
            if order_dict[security]['updated']:
                order_dict[security]['updated'] = False
                # TODO: update to handle non-ghost
                df = pd.DataFrame([[
                    order_dict[security]['accountID'], security, order_dict[security]['requestedPrice'],
                    order_dict[security]['side'], order_dict[security]['orderQty'], order_dict[security]['transactTime']
                    ]], columns=["accountID", "symbol", "price", "side", "quantity", "time"])
        sleep(2)


def lots_update_handler(value_dict, epics, trade_currencies):
    start = time()
    while True:
        if time() - start > 60:
            start = time()
            for security in value_dict:
                for account in account_info:
                    if value_dict[security]['accountID'] == account_info[account]['name'] and \
                        account_info[account]['balance'] != 0 and \
                            trade_currencies[epics[value_dict[security]['epic']]['currencyCode']] != 0:
                        value_dict[security]['lots'] = account_info[account]['balance'] * 0.01 * (trade_currencies[epics[value_dict[security]['epic']]['currencyCode']] ** value_dict[security]['lotexponent'])
                        value_dict[security]['lots'] = int((value_dict[security]['lots']/(value_dict[security]['pip']  * 4 *  value_dict[security]['perlot'])) * 100000)
        sleep(20)


def start_threads(value_dict: Tupperware, order_dict: Tupperware, security_bid_ask: Tupperware, currency_pairs: set):
    """Starts the thread handlers for various update and bid functionality"""
    start_new_thread(lots_update_handler, (value_dict, epics, trade_currencies))
    start_new_thread(quote_session_handler, (value_dict, security_bid_ask, currency_pairs, trade_currencies))
    start_new_thread(trade_session_handler, (order_dict,))
    start_new_thread(interface_handler, (order_dict,))
    start_new_thread(flat_zone, (value_dict, security_bid_ask))


def main():
    """Main executor function"""

    currency_pairs = set(list(epics.keys()) + list(trade_currencies.keys()))
    currency_pairs.remove("USDUSD")
    security_bid_ask = Tupperware({})

    field = ["BID", "OFFER"]
    mode = "MERGE"
    value_dict = Tupperware({})
    order_dict = Tupperware({})

    for index, item in enumerate(epics):

        round_ = 100 if epics[item]['pip'] == 0.01 else 10000

        # set the security bid
        security_bid_ask[item] = Tupperware({
            'index': index,
            'bid': 0.0,
            'ask': 0.0,
            'round': round_
        })

        value_dict[index] = Tupperware({
            'epic': item,
            'currencyCode': epics[item]['currencyCode'],
            'minute_current': Queue(),
            'minute_prev_current': datetime.now().minute,
            'minute_open': 0, 'minute_high': 0, 'minute_low': 0,
            'minute_close': 0, "minute_frame": [],

            'bollinger_low': [], 'bollinger_high': [], 'macd': [],
            'macdHisto': deque(maxlen=4),
            'macdSignal': 0, 'started': False,
            'pip': epics[item]['pip'],
            'perlot': epics[item]['perlot'],
            'ltp': Queue(), 'ask': 0, 'bid': 0, 'ltp_prev': 0,

            "ema_16": 0, "ema_28": 0, "risklevel": 0.01,
            "lots": 0, "ordered_lots": 0,
            "entry": 0, "stopfrac": 0,
            "stoploss": 0, "target": 0, "signal": position.NOPOS,
            'SLmulitple': 4,

            "accountID": epics[item]['accountID'],
            "position_filled": True, "long_position_count": 0,
            "short_position_count": 0, "stop_trades": False,
            'lotexponent': epics[item]['lotexponent'],
        })

        order_dict[item] = Tupperware({
            "updated": False,
            "accountID": epics[item]['accountID'],
            "clOrderID" : None, "orderID": None, "orderStatus": None,
            "execType": None, "side": 0, "orderQty": 0,
            "requestedPrice": 0, "remainingQty": 0, "cummQty": 0,
            "avgPrice": 0, "transactTime": None,
        })

    start_threads(value_dict, order_dict, security_bid_ask, currency_pairs)  # start all listeners
    forex(value_dict, epics, security_bid_ask, account_info)


if __name__ == "__main__":
    main()
