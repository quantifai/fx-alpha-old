from functools import reduce
import socket
import time
from time import sleep
from datetime import datetime
from _thread import start_new_thread
import ssl
import re
from queue import Queue


seq_quote_num = 0
seq_trade_num = 0

ack = False
logoutSession = False

# host = '216.93.241.29'
# port = 33712
# password = '8t9WaZNt'
# SenderQuoteCompID = 'PepperstoneInternal_Q'
# TargetQuoteCompID = 'OZ_Q'
# SenderTradeCompID = 'PepperstoneInternal_T'
# TargetTradeCompID = 'OZ_T'

host = 'Pepperstone-live.onezero.com'
port = 32235
SenderQuoteCompID = 'QuantifAI_Q'
TargetQuoteCompID = 'Pepperstone_Q'
SenderTradeCompID = 'QuantifAI_T'
TargetTradeCompID = 'Pepperstone_T'
password = '96w3CRLp'


price = 0
clorderID = 0
transact_time = None
loginSuccessful = True
fixData = Queue()
dataFormat = Queue()
open_position = {}
account_id_map = {'15': 'QuantifAI_1','16': 'QuantifAI_2','17': 'QuantifAI_3','18': 'QuantifAI_4','19': 'QuantifAI_5',
        '20': 'QuantifAI_6','21': 'QuantifAI_7','22': 'QuantifAI_8','23': 'QuantifAI_9','24': 'QuantifAI_10', }


def ConstructHeader(msg_type, SenderCompID, TargetCompID, seq_num, accountID=''):
    header = ''
    # global  seq_num
    seq_num += 1
    header += "8=FIX.4.4|"
    message = ''
    message += "35=" + msg_type + "|"
    message += "49="  + SenderCompID +  "|"
    message += "56="  + TargetCompID +  "|"
    message += "34="  + str(seq_num) +  "|"

    if accountID:
        message += "115=" + accountID + "|"

    message += "52="  + (datetime.utcnow()).strftime("%Y%m%d-%H:%M:%S.%f")[:-3] +  "|"
    return header, message, seq_num


def ConstructTrailer(headerAndBody):
    headerAndBody = str(headerAndBody.replace('|', chr(0x01)))
    chksum = str(reduce(lambda x,y:x+y, map(ord, headerAndBody)) % 256)
    chksum = chksum.zfill(3)
    return chksum


def MarketDataReq(subscription_id, SenderCompID, TargetCompID, seq_num):

    # Header
    msg_type = 'V'
    header, message, seq_num = ConstructHeader(msg_type, SenderCompID, TargetCompID, seq_num)
    body = "262="+subscription_id+"|263=1|264=1|265=0|146=1|55=EURUSD|267=2|269=0|269=1|"

    length = len(message) + len(body)
    header += "9=" + str(length) + "|"
    header = header + message
    headerAndBody = header + body

    chksum = ConstructTrailer(headerAndBody)
    trailer = "10="+str(chksum)+"|"
    headerAndBodyAndTrailer = headerAndBody + trailer
    print(headerAndBodyAndTrailer)
    headerAndBodyAndTrailer = headerAndBodyAndTrailer.replace("|", chr(0x01))
    return headerAndBodyAndTrailer, seq_num


def LogonMessage(heartBeatSeconds, password, resetSeqNum):
    body = ''
    # Encryption
    body += "98=0|"
    body += "108="  + heartBeatSeconds +  "|"

    if resetSeqNum:
        body += "141=Y|"
    body += "554=" + password + "|"
    return body


def LogonPacket(SenderCompID, TargetCompID, seq_num):
    msg_type = 'A'

    header, message, seq_num = ConstructHeader(msg_type, SenderCompID, TargetCompID, seq_num)
    heartBeatSeconds = '30'
    resetSeqNum = 1
    body = LogonMessage(heartBeatSeconds, password, resetSeqNum)
    length = len(message) + len(body)
    header += "9=" + str(length) + "|"
    header = header + message
    headerAndBody = header + body
    # Trailer
    chksum = ConstructTrailer(headerAndBody)
    trailer = "10="+str(chksum)+"|"
    headerAndBodyAndTrailer = headerAndBody + trailer
    print( 'ctrader packet is: ', headerAndBodyAndTrailer, '\n')
    headerAndBodyAndTrailer = headerAndBodyAndTrailer.replace("|", chr(0x01))
    return headerAndBodyAndTrailer, seq_num


def HeartBeat(text, SenderCompID, TargetCompID, seq_num):
    msg_type = '0'
    header, message, seq_num = ConstructHeader(msg_type, SenderCompID, TargetCompID, seq_num)
    if text:
        message += "112=" + text
    header += "9="+str(len(message)) + '|'
    header = header + message 
    chksum = ConstructTrailer(header)
    trailer = "10="+str(chksum)+"|"
    header += trailer
    print(header)
    return header.replace("|", chr(0x01)), seq_num


def LimitOrderReq(SenderCompID, TargetCompID, symbol, size, seq_num, side, accountID=''):
    msg_type = 'D'
    header, message, seq_trade_num = ConstructHeader(msg_type, SenderCompID, TargetCompID, seq_num, accountID)
    global clorderID, transact_time
    clorderID += 1
    transact_time = (datetime.utcnow()).strftime("%Y%m%d-%H:%M:%S.%f")[:-3]
    orderID = (datetime.now()).strftime("%Y_%m_%d_%H_%M_%S_%f") + "_" + str(clorderID)
    body = "11="+ orderID +"|21=1|"
    body += "55="+ str(symbol) +"|54=" + str(side)+ "|"
    body += "38=" + str(size) + "|"
    body += "40=1|44="+ str(price) +"|59=1|60="+  transact_time + "|"
    length = len(message) + len(body)
    header += "9=" + str(length) + "|"
    header = header + message
    headerAndBody = header + body
    chksum = ConstructTrailer(headerAndBody)
    trailer = "10="+str(chksum)+"|"
    headerAndBodyAndTrailer = headerAndBody + trailer
    print(headerAndBodyAndTrailer)
    return headerAndBodyAndTrailer.replace("|", chr(0x01)), seq_trade_num, orderID


def orderCancellation(SenderCompID, TargetCompID, origClorderID, seq_num, side, accountID=''):
    msg_type = 'F'
    header, message, seq_trade_num = ConstructHeader(msg_type, SenderCompID, TargetCompID, seq_num, accountID)
    global clorderID, transact_time
    clorderID += 1
    if transact_time:
        body = "41=" + str(origClorderID) + "|"
        body += "11=" + (datetime.now()).strftime("%Y_%m_%d_%H_%M_%S_%f") + "_" + str(clorderID) + "|"
        body += "54=" + str(side) + "|"
        body += "60=" + str(transact_time) + "|"
        length = len(message) + len(body)
        header += "9=" + str(length) +"|"
        header = header + message + body
        chksum = ConstructTrailer(header)
        header += "10=" + str(chksum) + "|"
        print(header)
        return header.replace("|", chr(0x01)), seq_trade_num


def logout(SenderCompID, TargetCompID, seq_num):
    msg_type = '5'
    header, message, seq_num = ConstructHeader(msg_type, SenderCompID, TargetCompID, seq_num)
    header += "9="+str(len(message)) + '|'  
    header = header + message 
    chksum = ConstructTrailer(header)
    trailer = "10="+str(chksum)+"|"
    header = header + trailer
    print(header)
    return header.replace("|", chr(0x01)), seq_num


def position_request(SenderCompID, TargetCompID, seq_num):
    msg_type = 'AN'
    header, message, seq_num = ConstructHeader(msg_type, SenderCompID, TargetCompID, seq_num)
    body = "710=" + str(int(time.time())) + "|"
    body += "724=0|453=1|448=*|"
    body += "60="+  (datetime.utcnow()).strftime("%Y%m%d-%H:%M:%S.%f")[:-3] + "|"
    header += "9="+str(len(message) + len(body)) + '|'  
    header = header + message + body
    chksum = ConstructTrailer(header)
    trailer = "10="+str(chksum)+"|"
    header = header + trailer
    print(header)
    return header.replace("|", chr(0x01)), seq_num


def account_info(SenderCompID, TargetCompID, seq_num):
    msg_type = 'BB'
    header, message, seq_num = ConstructHeader(msg_type, SenderCompID, TargetCompID, seq_num)
    body = "909=" + str(int(time.time())) + "|"
    body += "453=1|448=*|"
    header += "9="+str(len(message) + len(body)) + '|'  
    header = header + message + body
    chksum = ConstructTrailer(header)
    trailer = "10="+str(chksum)+"|"
    header = header + trailer
    print(header)
    return header.replace("|", chr(0x01)), seq_num


def trade_data():
    global ack, logoutSession, price, loginSuccessful, open_position
    while True:
        data = dataFormat.get()
        print(dict(data))
        data_feed = dict(data)
        if data_feed:
            if data_feed['35'] == 'AP':
                close_details = {}
                close_details['symbol'] = data_feed['55']
                close_details['longqty'] = int(data_feed['704'])
                close_details['shortqty'] = int(data_feed['705'])
                open_position[account_id_map[data_feed['448']]].append(close_details)

def tag_handler():
    data = fixData.get()
    data = data.decode()
    index = 0
    length = len(data)
    key = ''
    value = ''
    key_bool = True
    value_bool = False
    tags = []
    while True:
        if index == length:
            data = fixData.get()
            data = data.decode()
            index = 0
            length = len(data)

        if data[index] is chr(0x01):
            tags.append((key, value))

            if key == '10':
                dataFormat.put(tags)
                tags = []
            # if key in tags_required:
            key_bool = True
            value_bool = False
            key = ''
            value = ''
        elif data[index] is '=':
            key_bool = False
            value_bool = True
        elif key_bool is True:
            key += data[index]
        elif value_bool is True:
            value += data[index]
        index += 1


def heartbeatRes(sock):
    global logoutSession
    while True:
        try:
            fixData.put(sock.recv())
        except (socket.timeout, TimeoutError, ConnectionResetError) as e:
            print(e, "Error")
            pass
        if logoutSession == True:
            break


def Main():

    session_trade = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    for account in account_id_map.values():
        open_position[account] = []
    socketT = ssl.wrap_socket(session_trade, ssl_version=ssl.PROTOCOL_TLSv1)

    socketT.connect((host, port))
    start_new_thread(heartbeatRes, (socketT,))
    start_new_thread(tag_handler, ())
    start_new_thread(trade_data, ())

    global seq_quote_num, seq_trade_num, ack, logoutSession

    # headerAndBodyAndTrailer, seq_quote_num = LogonPacket(SenderQuoteCompID, TargetQuoteCompID, seq_quote_num)
    # print( 'LOGON packet sent to ctrader!!!')
    # socketQ.send(str.encode(headerAndBodyAndTrailer))

    headerAndBodyAndTrailer, seq_trade_num = LogonPacket(SenderTradeCompID, TargetTradeCompID, seq_trade_num)
    print( 'LOGON packet sent to ctrader!!!')
    socketT.send(str.encode(headerAndBodyAndTrailer))
    sleep(1)

    if loginSuccessful is False:
        exit()
    sleep(1)

    start = time.time()
    logoutTime = time.time()
    log = 0
    headerAndBodyAndTrailer, seq_trade_num = position_request(SenderTradeCompID, TargetTradeCompID, seq_trade_num)
    print( 'MARTKET_DATA_REQ packet sent to ctrader!!!')
    socketT.send(str.encode(headerAndBodyAndTrailer))

    headerAndBodyAndTrailer, seq_trade_num = account_info(SenderTradeCompID, TargetTradeCompID, seq_trade_num)
    print( 'MARTKET_DATA_REQ packet sent to ctrader!!!')
    socketT.send(str.encode(headerAndBodyAndTrailer))

    sleep(5)
    print(open_position)

    for account in open_position:
        for pos in open_position[account]:
            qty = 0
            if pos['longqty'] != 0:
                side = 2
                qty = pos['longqty']
            elif pos['shortqty'] != 0:
                side = 1
                qty = pos['shortqty']
            else:
                side = 0
            if side is not 0:
                headerAndBodyAndTrailer, seq_trade_num, orderID = LimitOrderReq(SenderTradeCompID, TargetTradeCompID, pos['symbol'], qty, seq_trade_num , side, account)
                socketT.send(str.encode(headerAndBodyAndTrailer))
            sleep(0.1)


    while True:
        if time.time() - start > 27:
            start = time.time()
            headerHeartBeat, seq_trade_num = HeartBeat("", SenderTradeCompID, TargetTradeCompID, seq_trade_num)
            socketT.send(str.encode(headerHeartBeat))

            if price is not 0 and log % 3 == 0:
                log += 1
               
            elif log % 3 == 1:
                log += 1
                headerAndBodyAndTrailer, seq_trade_num = position_request(SenderTradeCompID, TargetTradeCompID, seq_trade_num)
                print( 'MARTKET_DATA_REQ packet sent to ctrader!!!')
                socketT.send(str.encode(headerAndBodyAndTrailer))           
            elif log % 3 == 2:
                log += 1
                headerAndBodyAndTrailer, seq_trade_num = account_info(SenderTradeCompID, TargetTradeCompID, seq_trade_num)
                print( 'MARTKET_DATA_REQ packet sent to ctrader!!!')
                socketT.send(str.encode(headerAndBodyAndTrailer))                

        if ack == True:
            ack = False
            headerHeartBeat, seq_trade_num = HeartBeat("TEST|", SenderTradeCompID, TargetTradeCompID, seq_trade_num)
            socketT.send(str.encode(headerHeartBeat))

        if time.time() - logoutTime > 2000:
            headerlogout, seq_trade_num = logout(SenderTradeCompID, TargetTradeCompID, seq_trade_num)
            socketT.send(str.encode(headerlogout))
            break

        if logoutSession is True:  
            headerlogout, seq_trade_num = logout(SenderTradeCompID, TargetTradeCompID, seq_trade_num)
            socketT.send(str.encode(headerlogout))
            break

    sleep(10)
    socketT.close()


if __name__ == '__main__':
    Main()



